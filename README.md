
# CICD Library

**GitLab CICD configuration elements for inclusion in GitLab projects**

based on the [ProCICD library framework](https://procicd.gitlab.io)

## Usage

Projects utilize GitLab CICD by adding a `.gitlab-ci.yml` file with the following convention:

```yaml
include:
  - project: procicd/lib
    file: java/maven-pipe.gitlab-ci.yml
    ref: '2.0.0'
    inputs:
      skip-test: '.'
      maven-version: '3.8.6'
```

Values for the various keys include:

- `include:` ideally only contains one item in the project.
- `project:` always refers to this library at `%{library_project_path}`.
- `file:` the path within the library to the main pipeline configuration to be used by the project. Because a project requires an entire pipeline, the value will typically end in `-pipe.gitlab-ci.yml`.
- `ref:` (optional) locks down the version of the library being referenced, to avoid the risk that changes to the library break project pipelines. To find the latest release of the CICD library, see the [library releases page](https://gitlab.com/%{library_project_path}/-/releases).
- `inputs:` contain specific values that apply to the project in question. Review the pipeline configuration file (referenced by `file:` above) for descriptions of input keys.
