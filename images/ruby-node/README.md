# Roby-Node

A Docker image that includes both Ruby and Node of specific versions. Handy for Rails projects.

The resulting image name  will include the versions of both Ruby and Node in the tag, such as `ruby-node:ruby-3.0.4-node-18.15.0`

Then create and distribute a deploy key as per the [Token Tool](https://gitlab.com/fpotter/token-tool) or follow the [documentation](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#configure-a-job)
