# Docker Service

A Docker image for building other Docker images in GitLab CI, whether with Maven or the Docker CLI. Replaces the `docker:dind` service.

Just a scratchpad, showing how the certificate override might work in the case of a private registry.