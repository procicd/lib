# Maven image prebuild

Docker configuration to prebuild an image for use in GitLab CI jobs that work with Maven and a shared settings.xml file.

The resulting image name will include the versions of both the JDK and Docker in the tag, such as `maven:${JOB_IMAGE_VERSION}-jdk-8-mvn-3.6.3`

Includes the ability to deploy to the GitLab maven registry.

To use it, create and distribute a deploy key as per the [Token Tool](https://gitlab.com/procicd/token-tool) or follow the [documentation](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#configure-a-job)

