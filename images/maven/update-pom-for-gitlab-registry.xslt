<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="2.0">

    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/*[local-name()='project']">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
            <xsl:choose>
                <xsl:when test="*[local-name()='repositories']"/>
                <xsl:otherwise>
                    <repositories xmlns="http://maven.apache.org/POM/4.0.0">
                        <repository xmlns="http://maven.apache.org/POM/4.0.0">
                            <id>gitlab-maven</id>
                            <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
                        </repository>
                    </repositories>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="*[local-name()='distributionManagement']"/>
                <xsl:otherwise>
                    <distributionManagement xmlns="http://maven.apache.org/POM/4.0.0">
                        <repository>
                            <id>gitlab-maven</id>
                            <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
                        </repository>
                        <snapshotRepository>
                            <id>gitlab-maven</id>
                            <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
                        </snapshotRepository>
                    </distributionManagement>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/*[local-name()='project']/*[local-name()='repositories']">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
            <repository xmlns="http://maven.apache.org/POM/4.0.0">
                <id>gitlab-maven</id>
                <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
            </repository>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/*[local-name()='project']/*[local-name()='distributionManagement']">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
            <repository xmlns="http://maven.apache.org/POM/4.0.0">
                <id>gitlab-maven</id>
                <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
            </repository>
            <snapshotRepository xmlns="http://maven.apache.org/POM/4.0.0">
                <id>gitlab-maven</id>
                <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
            </snapshotRepository>
        </xsl:copy>
    </xsl:template>

    <xsl:strip-space elements="*" />

</xsl:stylesheet>