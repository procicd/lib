# Maven-Docker

A Docker image for building other Docker images in GitLab CI, whether with Maven or the Docker CLI. Requires the `docker:dind` service.

Effectively adds the Docker CLI to the `maven:3.8.5-openjdk-17-slim` image so it could be used for either, along with any additional functionality that's required for docker builds and pushes to the cloud registry.

Future option: Use this approach to distribute the `settings.xml` file to jobs within the `maven` image rather than pulling it from a CI/CD variable.

The resulting image name  will include the versions of both the JDK and Docker in the tag, such as `maven-docker:jdk-17-mvn-3.8.4`

Then create and distribute a deploy key as per the [Token Tool](https://gitlab.com/fpotter/token-tool) or follow the [documentation](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#configure-a-job)
