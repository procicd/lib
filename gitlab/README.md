Wrappers for so-called "templates" that ship with GitLab, including much of the product functionality like security scanning and kubernetes deployments.

Note that the "templates" are shipped with the product so they evolve very
little to maintain backward-compatability. Wrapping them here allows us to
control how they are implemented within our CICD library.
