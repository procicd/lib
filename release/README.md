# Release

CICD configuration files related to version numbering and release management.

The two are separate - for example, it's possible to see what the next version
number **would** be without actually releasing anything.